function __(id){
    return document.getElementById(id);
}

__("js-one").addEventListener("click", () => {
    let hours = (new Date()).getHours();
    let minutes = (new Date()).getMinutes();
    let seconds = (new Date()).getSeconds();
    let transformHours = hours * 3600;
    let transformMinutes = minutes * 60;
    let result = transformHours + transformMinutes + seconds;
    alert(`La hora actual es: ${hours}:${minutes}:${seconds}\nLa hora actual en segundos es: ${result}`);
});

__("js-two").addEventListener("click", () => {
    let base = prompt("Ingresa la base del triángulo:");
    let height = prompt("Ingresa la altura del triángulo:")
    let result = (base * height) / 2;
    alert(`El área del triángulo es: ${result}`);
});

__("js-three").addEventListener("click", () => {
    let number = prompt("Ingresa un número impar:");
    let result = Math.sqrt(number).toFixed(2);
    alert(`La raíz cuadrada de ${number} es: ${result}`);
});

__("js-four").addEventListener("click", () => {
    let words = prompt("Ingresa una frase:");
    alert(`La frase: ${words}\nContiene: ${words.length} caracteres`);
});

__("js-five").addEventListener("click", () => {
    let firstArray = ["Lunes", " Martes", " Miércoles", " Jueves", " Viernes"];
    let secondArray = [" Sábado", " Domingo"];
    let weekDays = firstArray.concat(secondArray);
    alert(`Los días de la semana son:\n${weekDays}`);
});

__("js-six").addEventListener("click", () => {
    alert(`La versión del navegador es: ${navigator.appVersion}`);
});

__("js-seven").addEventListener("click", () => {
    alert(`El ancho de la pantalla es de: ${screen.width} píxeles\nEl alto de la pantalla es de : ${screen.height} píxeles`);
});

__("js-eight").addEventListener("click", () => {
    if(window.print){
        window.print();
    }
});