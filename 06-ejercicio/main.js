/*
  Función para ahorrar codigo:
  document.getElementById() = __();
*/
function __(id){
    return document.getElementById(id);
}
/*
  Función que valida el formulario.
  Utiliza expresiones regulares sencillas.
*/
function validation(){
  var name = __('name').value,
      lname = __('lastname').value,
      mail = __('email').value,
      tel = __('number').value,
      sex = __('sex').selectedIndex,
      date = __('birthdate').value,
      password = __('password').value,
      confirm = __('confirm').value;
  if(name.length < 3 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(name))){
    alert('Debes escribir un nombre válido.');
    return false;
  }
  else if(lname < 3 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(lname))){
    alert('Debes escribir un apellido válido.');
    return false;
  }
  else if(!(/^[^@]+@[^@]+\.[a-zA-Z]{2,}$/.test(mail))){
    alert('Debes escribir un correo electrónico válido.');
    return false;
  }
  else if(!(/^\d{10}$/.test(tel))){
    alert('Debes escribir un número celular válido.');
    return false;
  }
  else if(sex == null || sex == 0){
    alert('Debes seleccionar un sexo.');
    return false;
  }
  else if(!(isNaN(date))){
    alert('Debes seleccionar una fecha de nacimiento válida.');
    return false;
  }
  else if(password < 8){
    alert('Debes ingresar una contraseña válida.');
    return false;
  }
  else if(confirm != password){
    alert('Las contraseñas no coinciden.');
    return false;
  }
  alert('El formulario se envió con éxito.')
  return true;
}