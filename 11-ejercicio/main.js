function __(id){
    return document.getElementById(id);
}
__("js-one").addEventListener("click", convertNumber);

function convertNumber(){
    let number = prompt("Introduce un valor numérico en base octal (8):");
    let numberOctal = parseInt(number, 8);
    if(isNaN(numberOctal)){
        windowError = window.open("","Nueva Ventana", "width=300px, height=200px, top=150, left=100");
        windowError.document.write("<title>¡Error 404!</title>");
        windowError.document.write("<p>El valor ingresado no es un número octal.</p>");
        windowError.document.write("<input type=\"button\" value=\"Cerrar ventana\" id=\"btn\">");
        windowError.document.getElementById("btn").addEventListener("click", () => {
            windowError.close();
        });
    }
    else{
        let numberDecimal = parseInt(numberOctal, 10)
        windowConvert = window.open("","Nueva Ventana", "width=300px, height=200px, top=150, left=100");
        windowConvert.document.write("<title>Conversión</title>");
        windowConvert.document.write(`<p>El valor octal es: ${number}<br>
                                El valor decimal es: ${numberDecimal} </p>`);
        windowConvert.document.write("<input type=\"button\" value=\"Cerrar ventana\" id=\"btn\">");
        windowConvert.document.getElementById("btn").addEventListener("click", () => {
            windowConvert.close();
        });
    }
}

__("js-two").addEventListener("click", operations);

function operations(){
    let numberOne = parseInt(prompt("Ingresa el primer valor:"));
    let numberTwo = parseInt(prompt("Ingresa el segundo valor:"));
    if(isNaN(numberOne) || isNaN(numberTwo)){
        alert("Debes ingresar dos números enteros.");
    }
    else{
        function mathematicalOperations(valorOne, valorTwo){
            let addition = valorOne + valorTwo;
            let subtraction = valorOne - valorTwo;
            let multiplication = valorOne * valorTwo;
            let division = valorOne / valorTwo;
            alert(`Los valores ingresados son: ${valorOne} y ${valorTwo}\nSuma: ${addition}\nResta: ${subtraction}\nMultiplicación: ${multiplication}\nDivisión: ${division}`);
        }
        mathematicalOperations(numberOne, numberTwo);
    }
}