function __(id){
    return document.getElementById(id);
}
__("js-one").addEventListener("click", () =>{
    var months = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    windowOne = window.open("","Nueva Ventana", "width=340px, height=280px, top=100, left=20");
    windowOne.document.write("<title>Meses</title>");
    for(let i=0; i<months.length; i++){
        windowOne.document.write(`${months[i]}<br>`);
    }
    windowOne.document.write("<p><input type=\"button\" value=\"Cerrar ventana\" id=\"btn\"></p>");
    windowOne.document.getElementById("btn").addEventListener("click", () => {
        windowOne.close();
    });
});

__("js-two").addEventListener("click", () =>{
    function printData(){
        windowTwo.document.write(`<b>Código:</b> ${this.code} | 
        <b>Nombre:</b> ${this.name} | 
        <b>Precio:</b> ${this.price} <br>`);
    }
    function foodProduct(code, name, price){
        this.code = code;
        this.name = name;
        this.price = price;
        this.printData = printData;
    }
    var products = new Array(3);
    products[0] = new foodProduct("A001", "Pepino", "1.50");
    products[1] = new foodProduct("B002", "Tomate", "0.80");
    products[2] = new foodProduct("C003", "Harina", "1.25");
    windowTwo = window.open("","Nueva Ventana", "width=340px, height=280px, top=100, left=20");
    windowTwo.document.write("<title>Productos</title>");
    for(let i=0; i<products.length; i++){
        products[i].printData();
    }
    windowTwo.document.write("<p><input type=\"button\" value=\"Cerrar ventana\" id=\"btn\"></p>");
    windowTwo.document.getElementById("btn").addEventListener("click", () => {
        windowTwo.close();
    });
});