/*
  Función para ahorrar codigo:
  document.getElementById() = __();
*/
function __(id){
    return document.getElementById(id);
}
/*
  Función que valida el formulario.
  Utiliza expresiones regulares sencillas.
*/
function validation(){
    var name = __('name').value,
        lname = __('lastname').value,
        mail = __('email').value,
        ci = __('ci').value,
        city = __('city').value,
        address = __('address').value,
        tel = __('number').value,
        sex = __('sex'),
        optionSex = sex.options[sex.selectedIndex].text,
        status = __('status'),
        optionStatus = status.options[status.selectedIndex].text;

    if(name.length < 3 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(name))){
      alert('Debes escribir un nombre válido.');
      return false;
    }
    else if(lname.length < 3 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(lname))){
      alert('Debes escribir un apellido válido.');
      return false;
    }
    else if(!(/^[^@]+@[^@]+\.[a-zA-Z]{2,5}$/.test(mail))){
      alert('Debes escribir un correo electrónico válido.');
      return false;
    }
    else if(!(/^\d{10}$/.test(ci))){
        alert('Debes escribir una cédula de identidad válida.');
        return false;
    }
    else if(!(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(city))){
        alert('Debes escribir una ciudad válida.');
        return false;
    }
    else if(address.length < 5 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(address))){
        alert('Debes escribir una dirección válida.');
        return false;
    }
    else if(!(/^\d{10}$/.test(tel))){
      alert('Debes escribir un número celular válido.');
      return false;
    }
    else if(sex.selectedIndex == null || sex.selectedIndex == 0){
      alert('Debes seleccionar un sexo.');
      return false;
    }
    else if(status.selectedIndex == null || status.selectedIndex == 0){
        alert('Debes seleccionar un estado civil.');
        return false;
    }
    setCookie('Name', name, 1);
    setCookie('LastName', lname, 1);
    setCookie('Mail', mail, 1);
    setCookie('IdentityCard', ci, 1);
    setCookie('City', city, 1);
    setCookie('Address', address, 1);
    setCookie('Phone', tel, 1);
    setCookie('Sex', optionSex, 1);
    setCookie('Status', optionStatus, 1);
    alert('El formulario se envió con éxito.');
    return true;
}

function setCookie(setName, setValue, expiration){
  var date = new Date();
  date.setTime(date.getTime()+expiration*24*60*60*1000);
  var expiration = 'expires='+date.toUTCString();
  document.cookie = setName+'='+setValue+';'+expiration+';path=/';
}

function getCookie(getName){
  var cookieName = getName+'=';
  var array = document.cookie.split(';');
  for(let i=0; i<array.length; i++){
     var cookie = array[i];
     while (cookie.charAt(0)==' '){
       cookie = cookie.substring(1);
     }
     if(cookie.indexOf(getName)==0){
       return cookie.substring(cookieName.length, cookie.length);
     }
  }
  return '';
}

__('btn').addEventListener('click', showCookie);

function showCookie(){
  if(document.cookie==''){
    alert('No existen cookies en el navegador.');
  }
  else{
    let name = getCookie('Name');
    let lname = getCookie('LastName');
    let mail = getCookie('Mail');
    let ci = getCookie('IdentityCard');
    let city = getCookie('City');
    let address = getCookie('Address');
    let phone = getCookie('Phone');
    let sex = getCookie('Sex');
    let status = getCookie('Status');
    alert('Nombre: '+name+ '\nApellido: '+lname+
          '\nEmail: '+mail+ '\nCédula de identidad: '+ci+
          '\nCiudad: '+city+ '\nDirección: '+address+
          '\nTeléfono: '+phone+ '\nSexo: '+sex+ '\nEstado civil: '+status);
  }
}