function getChildNodes(element) {
    let result = [];
    // Contar los hijos que tiene
    for (var i = 0; i < element.childNodes.length; i++) {
        result.push(element.childNodes[i]);
        // Si tiene hijos
        if(element.childNodes[i].hasChildNodes()) {
            // Llamamos a la función nuevamente
            result = result.concat(getChildNodes(element.childNodes[i]));
        }
    }
    return result;
}

function showNodes(){
    const element = document.documentElement;
    const result = getChildNodes(element);
    result.unshift(document.documentElement);
    for (i in result){
        if (result[i].nodeType==3) {
            alert(result[i].textContent);
        }
        else{
            alert(result[i].nodeName.toLowerCase());
        }
    }
}

document.getElementById('btn').addEventListener('click', showNodes);