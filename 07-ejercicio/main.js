function __(id){
    return document.getElementById(id);
}
__("js-one").addEventListener("click", number);

function number(){
    var maxValue = Number.MAX_VALUE;
    var minValue = Number.MIN_VALUE;
    alert("Valor máximo: " + maxValue);
    alert("Valor mínimo: " + minValue);
    alert("Valor especial: " + maxValue*2);
}

__("js-two").addEventListener("click", operators);

function operators(){
    let operator = prompt("Introduce el operador a evaluar AND, OR o NOT.");
    operator = operator.toLowerCase();
    if(operator == "and"){
        let operando_uno = eval(prompt("Introduce el primer valor lógico (true o false):", true));
        let operando_dos = eval(prompt("Introduce el segundo valor lógico (true o false):", true));
        let resultado_logico = operando_uno && operando_dos;
        alert("Resultado: " + resultado_logico);
    }
    else if(operator == "or"){
        let operando_uno = eval(prompt("Introduce el primer valor lógico (true o false):", true));
        let operando_dos = eval(prompt("Introduce el segundo valor lógico (true o false):", true));
        let resultado_logico = operando_uno || operando_dos;
        alert("Resultado: " + resultado_logico);
    }
    else if(operator == "not"){
        let operando_uno = eval(prompt("Introduce un valor lógico (true o false):", true));
        let resultado_logico = !operando_uno;
        alert("Resultado: " + resultado_logico);
    }
    else if(operator == ""){
        alert("Debes ingresar un operador lógico.");
    }
    else{
        alert("Debes ingresar el operador lógico de manera correcta.");
    }
}

__("js-three").addEventListener("click", countDown);

function countDown(){
    let number = prompt("Introduce un número para iniciar la cuenta atrás:");
    while (number > 0){
        alert(number);
        number--;
    }
}

__("js-four").addEventListener("click", countDownFor);

function countDownFor(){
    let number = prompt("Introduce un número para iniciar la cuenta atrás:");
    for(number; number > 0; number--){
        alert(number);
    }
}