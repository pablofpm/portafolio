function __(id){
    return document.getElementById(id);
}
__("js-one").addEventListener("click", hello);

function hello(){
    var primerSaludo = "Hola mundo.";
    var segundoSaludo = primerSaludo;
    primerSaludo = "Hello World.";
    alert(segundoSaludo);
}

__("js-two").addEventListener("click", division);

function division(){
    var dividendo = prompt("Introduce el dividendo:");
    var divisor = prompt("Introduce el divisor:");
    var resultado;
    divisor !=0? resultado = dividendo / divisor:
    alert("No es posible la división por cero.");
    if (divisor > 0){
        alert("El resultado es: " + resultado);
    }
}